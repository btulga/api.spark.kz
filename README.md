# Api.Spark.Kz

## Usage

```php
<?php 

...

use Alphalabs\ApiSparkKz\Client as SparkClient;

class ...SomeClass {
  ...
}
```

## Create client

```php
// for production
$client = new SparkClient('https://gateway.spark.kz');

// for testing
$client = new SparkClient('https://gateway.spark-dev.team');
```

## Authenticate

```php
$clientId = 'someClientId';
$clientSecret = 'someClientId';

['expires_in' => $expiresIn, 'access_token' => $token] = $client->authorize([
    'client_id' => $clientId,
    'client_secret' => $clientSecret
]); 

echo $token; // access token
echo $expiresIn // expires in seconds

```

## Set access token

```php
// set access token
$client->setAccessToken($token);

````

## Create order

```php 

$request = [
    "sender" => [
        "title" => "test",
        "city_id" => 1,
        "full_address" => "Abaya 12",
        "full_name" => "Hello",
        "phone" => "+77083039131",
        "office" => "1",
        "comment" => "test",
        "index" => "1",
        "house" => "1",
        "street" => "1",
        "self-delivery" => "1"
    ],
    "receivers" => [
        [
            "title" => "test sender",
            "city_id" => 3,
            "full_address" => "Abaya 13",
            "latitude" => "",
            "longitude" => "",
            "full_name" => "Test",
            "phone" => "+77083039131",
            "office" => "1",
            "comment" => "1",
            "index" => "1",
            "house" => "1",
            "street" => "1",
            "self-delivery" => 1,
            "additional_service" => [
                "hasLoader" => true,
                "hasSoftPackage" => true,
                "hasManipulator" => true,
                "hasCrane" => true,
                "hasCar" => true,
                "hasHydraulicTrolley" => true,
                "hasGrid" => true
            ],
            "order_logistics_info" => [
                "invoice_number" => "SP000001",
                "product_name" => "product_name",
                "places" => 1,
                "volume" => 1,
                "should_return_document" => 1,
                "height" => 1,
                "width" => 1,
                "depth" => 1,
                "weight" => 1,
                "annotation" => "1",
                "payment_type" => 1,
                "payment_method" => 1,
                "shipment_type" => 1,
                "cod_payment" => 1,
                "declared_price" => 1,
                "take_date" => "2022-05-12",
                "period_id" => 1,
                "verify" => true,
                "track_number" => ""
            ]
        ],
        [
            "title" => "test sender",
            "city_id" => 3,
            "full_address" => "Abaya 13",
            "latitude" => "",
            "longitude" => "",
            "full_name" => "Test",
            "phone" => "+77083039131",
            "office" => "1",
            "comment" => "1",
            "index" => "1",
            "house" => "1",
            "street" => "1",
            "self-delivery" => 1,
            "additional_service" => [
                "hasLoader" => true,
                "hasSoftPackage" => true,
                "hasManipulator" => true,
                "hasCrane" => true,
                "hasCar" => true,
                "hasHydraulicTrolley" => true,
                "hasGrid" => true
            ],
            "order_logistics_info" => [
                "invoice_number" => "SP000001",
                "product_name" => "product_name",
                "places" => 1,
                "volume" => 1,
                "should_return_document" => 1,
                "height" => 1,
                "width" => 1,
                "depth" => 1,
                "weight" => 1,
                "annotation" => "1",
                "payment_type" => 1,
                "payment_method" => 1,
                "shipment_type" => 1,
                "cod_payment" => 1,
                "declared_price" => 1,
                "take_date" => "2022-05-12",
                "period_id" => 1,
                "verify" => true,
                "track_number" => ""
            ]
        ]
    ],
    "additional_service" => [
        "hasLoader" => true,
        "hasSoftPackage" => true,
        "hasManipulator" => true,
        "hasCrane" => true,
        "hasCar" => true,
        "hasHydraulicTrolley" => true,
        "hasGrid" => true
    ]
];

$result = $client->createOrder($request); 
 ```

#### sample response

```json
{
  "data": {
    "order_id": 624332,
    "invoices": [
      {
        "id": 710301,
        "title": "test sender",
        "invoice_number": "SP00078478",
        "track_number": null,
        "latitude": "47.1105353",
        "longitude": "51.9646138",
        "full_address": "Kazakhstan, Atyrau, Kuttygai Batyr street (Elevatornaya), 8"
      },
      {
        "id": 710302,
        "title": "test sender",
        "invoice_number": "SP00078479",
        "track_number": null,
        "latitude": "47.1105353",
        "longitude": "51.9646138",
        "full_address": "Kazakhstan, Atyrau, Kuttygai Batyr street (Elevatornaya), 8"
      }
    ]
  },
  "code": 0,
  "success": true,
  "message": "Order created!",
  "errors": []
}
```