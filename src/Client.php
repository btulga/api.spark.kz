<?php

namespace Alphalabs\ApiSparkKz;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;

class Client
{
    public GuzzleClient $client;

    private string $accessToken;

    public function __construct($baseUri)
    {
        $this->client = new GuzzleClient([
            'base_uri' => $baseUri,
        ]);
    }

    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * Make request
     *
     * @param $method
     * @param $uri
     * @param array $options
     *
     * @return array
     */
    protected function doRequest($method, $uri, array $options = []): array
    {
        try {
            // set defaults
            $default = [];
            if (isset($this->accessToken)) {
                $default['headers'] = ['Authorization' => "Bearer {$this->accessToken}"];
            }
            $options = array_replace($default, $options);

            $response = $this->client->request($method, $uri, $options);

            return json_decode((string)$response->getBody(), true, 512, \JSON_THROW_ON_ERROR);
        } catch (ClientException $e) {
            return json_decode((string)$e->getResponse()->getBody(true), true, 512, \JSON_THROW_ON_ERROR);
        }
    }

    /**
     * @param array $request
     * @return array
     * @throws \JsonException
     */
    public function authorize(array $request): array
    {
        return $this->doRequest('POST', 'oauth/token', [
            'form_params' => array_merge(['grant_type' => 'service'], $request),
            'headers' => [],
        ]);
    }

    /**
     * @param $request
     * @return array
     * @throws \JsonException
     */
    public function calculateCost($request): array
    {
        return $this->doRequest('POST', "billing/api/v2/calculators/client", [
            'json' => $request,
        ]);
    }

    /**
     * Cancel an invoice
     * @param $invoiceNumber
     * @return array
     * @throws \JsonException
     */
    public function cancelInvoice($invoiceNumber): array
    {
        return $this->doRequest('POST', "cabinet/api/v2/logistics-info/invoice/{$invoiceNumber}/cancel");
    }


    /**
     * @param $orderId
     * @return array
     * @throws \JsonException
     */
    public function cancelOrder($orderId): array
    {
        return $this->doRequest('POST', "cabinet/api/v2/orders/{$orderId}/cancel");
    }


    /**
     * @param $orderId
     * @return array
     * @throws \JsonException
     */
    public function getCargoDetail($orderId): array
    {
        return $this->doRequest('GET', "cabinet/api/client/order/{$orderId}/invoice-cargo");
    }

    /**
     * @param array $request
     * @return array
     * @throws \JsonException
     */
    public function getCities(array $request): array
    {
        return $this->doRequest('GET', 'cabinet/api/v2/cities', [
            'query' => $request
        ]);
    }


    /**
     * @param $invoiceNumber
     * @return array
     * @throws \JsonException
     */
    public function getCostDetail($invoiceNumber): array
    {
        return $this->doRequest('GET', "cabinet/api/v2/logistics-info/{$invoiceNumber}/get-cost-client");
    }

    /**
     * @param array $request
     * @return array
     * @throws \JsonException
     */
    public function getCountries(array $request): array
    {
        return $this->doRequest('GET', 'cabinet/api/v2/countries', [
            'query' => $request
        ]);
    }

    /**
     * @param array $request
     * @return array
     * @throws \JsonException
     */
    public function createOrder(array $request): array
    {
        return $this->doRequest('POST', 'cabinet/api/client/order', [
            'json' => $request,
        ]);
    }

    /**
     * @param array $request
     * @return array
     * @throws \JsonException
     */
    public function getDirections(array $request): array
    {
        return $this->doRequest('GET', 'cabinet/api/direction', [
            'query' => $request
        ]);
    }

    /**
     * @param $cityId
     * @return array
     * @throws \JsonException
     */
    public function getDirectionCities($cityId): array
    {
        return $this->doRequest('GET', "cabinet/api/direction/get_where_cities/{$cityId}");
    }


    /**
     * @param $invoiceNumber
     * @return array
     * @throws \JsonException
     */
    public function getInvoiceStatus($invoiceNumber)
    {
        return $this->doRequest('GET', "cabinet/api/invoice-status/{$invoiceNumber}");
    }

    /**
     * @param $orderId
     * @return array
     * @throws \JsonException
     */
    public function getOrderStatus($orderId)
    {
        return $this->doRequest('GET', "cabinet/api/v2/orders/{$orderId}/statuses");
    }

    /**
     * @param $invoiceNumber
     * @param $request
     * @return array
     * @throws \JsonException
     */
    public function editInvoice($invoiceNumber, $request): array
    {
        return $this->doRequest('PUT', "cabinet/api/client/invoice/{$invoiceNumber}", [
            'json' => $request
        ]);
    }
}