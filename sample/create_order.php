<?php

require 'vendor/autoload.php';

$clientId = '';
$clientSecret = '';
$baseUrl = 'https://gateway.spark-dev.team';

$client = new \Alphalabs\ApiSparkKz\Client($baseUrl);

$resp = $client->authorize([
    'client_id' => $clientId,
    'client_secret' => $clientSecret,
]);

$token = $resp['access_token'];

// set access token
$client->setAccessToken($token);

// our invoice number
$invoiceNumber = 'EMALL_SP000001'.rand(100, 999);
$takeDate = '2024-05-12';

$request = [
    "sender" => [
        "title" => "test",
        "city_id" => 1,
        "full_address" => "Abaya 12",
        "full_name" => "Hello",
        "phone" => "+77083039131",
        "office" => "1",
        "comment" => "test",
        "index" => "1",
        "house" => "1",
        "street" => "1",
        "self-delivery" => "1"
    ],
    "receivers" => [
        [
            "title" => "test sender",
            "city_id" => 3,
            "full_address" => "Abaya 13",
            "latitude" => "",
            "longitude" => "",
            "full_name" => "Test",
            "phone" => "+77083039131",
            "office" => "1",
            "comment" => "1",
            "index" => "1",
            "house" => "1",
            "street" => "1",
            "self-delivery" => 1,
            "additional_service" => [
                "hasLoader" => true,
                "hasSoftPackage" => true,
                "hasManipulator" => true,
                "hasCrane" => true,
                "hasCar" => true,
                "hasHydraulicTrolley" => true,
                "hasGrid" => true
            ],
            "order_logistics_info" => [
                "invoice_number" => $invoiceNumber,
                "product_name" => "product_name",
                "places" => 1,
                "volume" => 1,
                "should_return_document" => 1,
                "height" => 1,
                "width" => 1,
                "depth" => 1,
                "weight" => 1,
                "annotation" => "1",
                "payment_type" => 1,
                "payment_method" => 1,
                "shipment_type" => 1,
                "cod_payment" => 1,
                "declared_price" => 1,
                "take_date" => $takeDate,
                "period_id" => 1,
                "verify" => true,
                "track_number" => ""
            ]
        ],
    ],
    "additional_service" => [
        "hasLoader" => true,
        "hasSoftPackage" => true,
        "hasManipulator" => true,
        "hasCrane" => true,
        "hasCar" => true,
        "hasHydraulicTrolley" => true,
        "hasGrid" => true
    ]
];

$result = $client->createOrder($request);

var_dump($result);