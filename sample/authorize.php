<?php

require 'vendor/autoload.php';


$clientId = '';
$clientSecret = '';
$baseUrl = 'https://gateway.spark-dev.team';

$client = new \Alphalabs\ApiSparkKz\Client($baseUrl);

$resp = $client->authorize([
    'client_id' => $clientId,
    'client_secret' => $clientSecret,
]);

var_dump($resp);